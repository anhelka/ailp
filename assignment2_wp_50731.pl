% candidate_number(50731).

% Find hidden identity by repeatedly calling agent_ask_oracle(oscar,o(1),link,L)
% find_identity(-A)
find_identity(A):-
  (part_module(2)   -> find_identity_2(A)
  ; otherwise -> find_identity_o(A)
  ).

find_identity_2(A):-
  bagof(Ac, actor(Ac), As), 
  eliminate(As, X). 
  
  % agent_ask_oracle(oscar, o(1), link, L),
  % wp(A, WT), 
  % setof(actor(A), member(L, wt_link(wp(A, WT), Ls)), S).

zip([], [], []).
zip([X|Xs], [Y|Ys], [[X,Y]|Zs]) :- zip(Xs,Ys,Zs).

% eliminate(+As, -Sss) 
eliminate(As, X):-
  agent_ask_oracle(oscar, o(1), link, L), 
  include(isAMember(L), As, Xs),
  % writeln(Xs),
  ( oneElem(Xs, X),!
  ; otherwise -> eliminate(Xs, X)).

oneElem([Xs], Xs).

isAMember(L, X):-
  bagof(AL, (wp(X, WT), wt_link(WT, AL)), Ls), 
  member(L, Ls).

find_identity_o(A):-
  A='Not yet implemented'.
