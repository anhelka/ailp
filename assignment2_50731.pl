candidate_number(50731).

solve_task(Task,Cost):-
  my_agent(Agent),
  query_world( agent_current_position, [Agent,P] ),
  solve_task_astar(Task,P,[],R,0,_NewPos),!,  % prune choice point for efficiency
  reverse(R,[_Init|Path]),
  query_world( agent_do_moves, [Agent,Path] ).

%%%%%%%%%% Useful predicates %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% backtracking depth-first search, needs to be changed to agenda-based A*
% solve_task_bt(Task,Current,Depth,RPath,[cost(Cost),depth(Depth)],NewPos) :-
%   achieved(Task,Current,RPath,Cost,NewPos).

% solve_task_bt(Task,Current,D,RR,Cost,NewPos) :-  
%   Current = [c(F,P)|RPath],
%   search(P,P1,R,C),
%   \+ memberchk(R,RPath),  % check we have not been here already
%   D1 is D+1,
%   F1 is F+C,
%   solve_task_bt(Task,[c(F1,P1),R|RPath],D1,RR,Cost,NewPos).  % backtrack search

achieved(go(Exit),Current,RPath,Cost,NewPos) :-
  % Current = [c(Cost,NewPos)|RPath],
  ( Exit=none -> true
  ; otherwise -> RPath = [Exit|_]
  ).
achieved(find(O),Current,RPath,Cost,NewPos) :-
  % Current = [c(Cost,NewPos)|RPath],
  ( O=none    -> true
  ; otherwise -> RPath = [Last|_],map_adjacent(Last,_,O)
  ).


% search(F,N,N,1) :-
%   map_adjacent(F,N,empty).

%%%%%%%%%%%%%%%%%%%% A* Bredth first search %%%%%%%%%%%%%%%%%%%%%%
% solve_task_astar(+Task, +Current, +PList, +RPath, -NewPos)
% solve_task_astar(Task, Current, PLIst, RPath, Cost, NewPos) :-
%   achieved(Task, Current, RPath, Cost, NewPos),
%   writeln('Things have kept going..\n').

% solve_task_astar(Task, Current, PList, RPath, Cost, NewPos) :-
%   % achieved(Task, Current, RPath, Cost, NewPos), 
%   NewCost is Cost + 1, 
%   writeln('Or is it here?'), 
%   search(Task, Current, Cost, RPath, Ns), 
%   merge_set(Ns, PList, NewPList),
%   head(NewPList, Next), head(Next, H),
%   append([H], RPath, NewRPath), 
%   writeln(NewRPath), 
%   solve_task_astar(Task, H, NewPList, NewRPath, NewCost, H).

solve_task_astar(Task, [Task,B,C,D], PList, NewPos) :- writeln(C), !.

solve_task_astar(Task, Current, PList, NewPos) :-
    split(Current,Node,N,RPath,Cost),
    NewCost is Cost+1,
    search(Task, Node, NewCost, RPath, Ns),
    writeln("NS"),
    writeln(Ns),
    merge_two_lists(Ns, PList, NewPList),
    head(NewPList, Next),
    tail(NewPList, NewerPList),
    solve_task_astar(Task, Next, NewerPList, H).


split([A,B,C,D],A,B,C,D).
search(Target, Current, Cost, RPath, Ns):-
findall([NextN, P, NewRPath,Cost] , ( map_adjacent(Current, NextN, O),
                             \+ O = t(_M),
                             \+ memberchk(NextN, RPath),
                             map_distance(NextN, Target, D),
                             P is D + Cost,
                             append(RPath,[NextN],NewRPath)
                             ), Ms),merge_sort_list(Ms,Ns).


merge_two_lists(_A,[],_A).
merge_two_lists([],_A,_A).
merge_two_lists([[P1,Q1,Z1,A1]|T1], [[P2,Q2,Z2,A2]|T2], [[P1,Q1,Z1,A1]|T]) :- Q1 =< Q2, merge_two_lists(T1,[[P2,Q2,Z2,A2]|T2],T ).
merge_two_lists([[P1,Q1,Z1,A1]|T1], [[P2,Q2,Z2,A2]|T2], [[P2,Q2,Z2,A2]|T]) :- Q1 > Q2, merge_two_lists([[P1,Q1,Z1,A1]|T1],T2,T ).
merge_sort_list([],[]).
merge_sort_list([X],[X]).
merge_sort_list(List,Sorted):-
  List = [_,_| _], divide(List,L1,L2),
  merge_sort_list(L1,Sorted1), merge_sort_list(L2,Sorted2),
  merge(Sorted1,Sorted2, Sorted).


head([X|_Xs], X).

merge([],L,L).
merge(L,[],L):- L \= [].
merge([[P1,Q1,Z1,A1]|T1],[[P2,Q2,Z2,A2]|T2],[[P1,Q1,Z1,A1]|T]) :- Q1 =< Q2, merge(T1,[[P2,Q2,Z2,A2]|T2], T ).
merge([[P1,Q1,Z1,A1]|T1],[[P2,Q2,Z2,A2]|T2],[[P2,Q2,Z2,A2]|T]) :- Q1 > Q2, merge([[P1,Q1,Z1,A1]|T1],T2, T ).

divide(L,L1,L2):-halve(L,L1,L2).
halve(L,A,B):-hv(L,[],A,B).

hv(L,L,[],L).      % for lists of even length
hv(L,[_|L],[],L).  % for lists of odd length
hv([H|T],Acc,[H|L],B):-hv(T,[_|Acc],L,B).

% https://kti.mff.cuni.cz/~bartak/prolog/recursion.html
% https://kti.mff.cuni.cz/~bartak/prolog/sorting.html
adj_list(Pos,List) :-
findall(A, map_adjacent(Pos, A, O), List).

remove_visited(InList, VisitedList,OutList).

achieved(go(Exit),Current,RPath,Cost,NewPos) :-
  Current = [c(Cost,NewPos)|RPath],
  ( Exit=none -> true
  ; otherwise -> RPath = [Exit|_]
  ).
achieved(find(O),Current,RPath,Cost,NewPos) :-
  Current = [c(Cost,NewPos)|RPath],
  ( O=none    -> true
  ; otherwise -> RPath = [Last|_],map_adjacent(Last,_,O)
  ).

% search(F,N,N,1) :-
%   map_adjacent(F,N,empty).

head([X|_Xs], X).
tail([_|Xs],Xs).